//Find all users with masters Degree.

function displayUserWithMAsterDegree(data) {

    try {
        for (let user in data) {
            if (data[user].qualification == "Masters") {
                console.log(user)
            }
        }
    } catch (error) {
        console.log(error)
    }
}

export default displayUserWithMAsterDegree